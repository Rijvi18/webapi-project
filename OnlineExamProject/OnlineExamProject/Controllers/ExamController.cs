﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OnlineExamProject.Models;


namespace OnlineExamProject.Controllers
{
    public class ExamController : ApiController
    {
        [HttpGet]
        public IHttpActionResult onlineexam()
        {
            ZooDbEntities db = new ZooDbEntities();
            IList<Onlineexamclass> oe=db.onlineExams.Include("onlineExam").Select(x=> new Onlineexamclass()
            {
                Qid=x.Qid,
                Question=x.Question,
                Option1=x.Option1,
                Option2=x.Option2,
                Option3=x.Option3,
                Option4=x.Option4,
                CorrectAns=x.CorrectAns
            }).ToList<Onlineexamclass>();
            return Ok(oe);
        }

    }
}
