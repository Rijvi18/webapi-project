﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineExamProject.Models;
using System.Net.Http;

namespace OnlineExamProject.Controllers
{
    public class ExamonlineprojectController : Controller
    {
        // GET: Examonlineproject
        public ActionResult Index()
        {
            IEnumerable<Onlineexamclass> oec = null;
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44354/api/exam");
            var consume = hc.GetAsync("Exam");
            consume.Wait();
            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                var display = test.Content.ReadAsAsync<IList<Onlineexamclass>>();
                display.Wait();
                oec = display.Result;
            }
            return View(oec);
        }
    }
}