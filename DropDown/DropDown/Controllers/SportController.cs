﻿using System.Web.Http;
using DropDown.Models;

namespace DropDown.Controllers
{
    public class SportController : ApiController
    {
        public IHttpActionResult insertsports(Sportsclass sp)
        {
            ZooDbEntities db = new ZooDbEntities();
            db.Sports.Add(new Sport()
            {
                SportsId = sp.Sportid,
                SportsName = sp.SportsName
            });
            db.SaveChanges();
            return Ok();
        }
    }
}
