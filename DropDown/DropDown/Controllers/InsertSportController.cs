﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DropDown.Models;
using System.Net.Http;

namespace DropDown.Controllers
{
    public class InsertSportController : Controller
    {
        // GET: InsertSport
        public ActionResult Index()
        {
            return View();
        }                                                    
        [HttpPost]
        public ActionResult Index(Sportsclass sp)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44383/api/Sport");
            var consume = hc.PostAsJsonAsync<Sportsclass>("Sport", sp);
            consume.Wait();

            var test = consume.Result;
            if (test.IsSuccessStatusCode)
            {
                ViewBag.message = sp.SportsName + "Is saved successfully";
            }
            return View();
        }
    }
}